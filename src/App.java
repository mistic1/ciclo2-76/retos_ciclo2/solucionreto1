public class App {
    public static void main(String[] args) throws Exception {
        Concesionario objConcesionario = new Concesionario();
        Concesionario objConcesionario2 = new Concesionario(0, 0, 0);
        System.out.println("El interes Simple es de: $"+objConcesionario.calcularInteresSimple(12,1200000,0.1));
        System.out.println("El interes Compuesto es de: $"+objConcesionario.calcularInteresCompuesto(12,1200000,0.1)+"\n");
        System.out.println(objConcesionario.compararInvConcesionario(12,1200000,0.1));
        System.out.print("\n");
        // System.out.println("el interes Simple es de: $"+objConcesionario2.calcularInteresSimple());
        // System.out.println("el interes Compuesto es de: $"+objConcesionario2.calcularInteresCompuesto());
        System.out.println(objConcesionario2.compararInvConcesionario(12, 1200000, 0.1)+"\n");
        
        
    }
}
