/**
* Universidad Tecnológica de Pereira <br>
* (Pereira, Risaralda - Colombia)
* */
/**
* Clase que representa un Proyecto.
*/
public class Concesionario {
    /****
    * Atributos
    ****/
    private int tiempo;
    private double capital;
    private double interes;
    /***
     * constructor
     ***/
    public Concesionario(){
        this.tiempo = tiempo;
        this.capital = capital;
        this.interes = interes;
    }
    public Concesionario(int tiempo, double capital, double interes){
        this.tiempo = tiempo;
        this.capital = capital;
        this.interes = interes;
    }
    /***
     * Metodos
     ***/
     public double calcularInteresSimple(int tiempo, double capital, double interes){
        double interesSimple = capital*interes*tiempo;
        return interesSimple;
    }
    public double calcularInteresCompuesto(int tiempo, double capital, double interes){
        double interesCompuesto = capital*((Math.pow((1+interes), tiempo))-1);
        return interesCompuesto;
    }
    public String compararInvConcesionario(int tiempo, double capital, double interes){
        
        double respuesta = (capital*((Math.pow((1+interes), tiempo))-1)) - (capital*interes*tiempo);
        if (tiempo >0 && capital >0 && interes >0){
            return "La diferencia en el total de intereses generados para el proyecto, si escogemos entre evaluarlo a una tasa de interés Compuesto y evaluarlo a una tasa de interés Simple, asciende a la cifra de: $"+respuesta;
        }
        else{
            return "Faltan datos para calcular la diferencia en el total de intereses generados para el proyecto.";
        }
    }
}

